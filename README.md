# fcf-pomodoro-clock

A pomodoro clock build in React.js and based on a FCF's project.

The notification sound is taken from
[Aarni Koskela's notification project](https://github.com/akx/Notifications)
which is distributed under CC BY 3.0.
