import React from 'react';
import './App.scss';
import { Container, Row } from '../BootstrapGrid';
import LengthContainer from './LengthContainer';
import Timer from './Timer';
import Controls from './Controls';

function setPreciseInterval(func, milliSecs, deltaFrac=0.2) {
  let startTime = new Date();
  return setInterval(() => {
    if (new Date() - startTime > milliSecs) {
      func()
      startTime = new Date();
    }
  }, milliSecs * deltaFrac);
}

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.startingState = {
      breakLength: 5,
      sessionLength: 25,
      timerType: 'Session',
      time: 1500,
      isStarted: false,
      intervalId: null,
    };
    this.state = Object.assign({}, this.startingState);
  }

  decrementPositiveState = (name) => () => {
    if (this.state.isStarted) {
      return;
    }
    this.setState((s) => {
      let newTime = s.time;
      if (s[name] <= 1) {
        return s;
      }
      if (name.match(/([a-z]+)[A-Z]/)[1]
        === s.timerType.toLowerCase()) {
        newTime = (s[name] - 1) * 60;
      }
      return { time: newTime, [name]: s[name] - 1 };
    });
  }

  incrementState = (name) => () => {
    if (this.state.isStarted) {
      return;
    }
    this.setState((s) => {
      let newTime = s.time;
      if (s[name] >= 60) {
        return s;
      }
      if (name.match(/([a-z]+)[A-Z]/)[1]
        === s.timerType.toLowerCase()) {
        newTime = (s[name] + 1) * 60;
      }
      return { time: newTime, [name]: s[name] + 1 };
    });
  }

  handleToggleStart = () => {
    if (this.state.isStarted) {
      this.setState((s) => {
        clearInterval(s.intervalId);
        return {
          intervalId: null,
          isStarted: false,
        };
      });
    } else {
      this.setState({
        intervalId: setPreciseInterval(this.decrementTime, 1000),
        isStarted: true,
      });
    }
  }

  handleReset = () => {
    if (this.state.intervalId) {
      clearInterval(this.state.intervalId);
    }
    const notification = document.getElementById('beep');
    notification.pause();
    notification.currentTime = 0;
    this.setState(this.startingState);
  }

  decrementTime = () => {
    this.setState((s) => {
      if (s.time === 0) {
        document.getElementById('beep').play();
        let newTimerType = 'Session';
        let newTime = s.sessionLength * 60;
        if (s.timerType === 'Session') {
          newTimerType = 'Break';
          newTime = s.breakLength * 60;
        }
        return { time: newTime, timerType: newTimerType } ;
      }
      return { time: s.time - 1 };
    });
  }

  render() {
    return (
      <Container
        as="main"
        className="bg-light rounded border p-4"
      >
        <Row className="d-flex justify-content-center">
          <h1>Pomodoro Clock</h1>
        </Row>
        <Row>
          <LengthContainer
            breakLength={this.state.breakLength}
            sessionLength={this.state.sessionLength}
            onBreakLengthDecrement={this.decrementPositiveState('breakLength')}
            onBreakLengthIncrement={this.incrementState('breakLength')}
            onSessionLengthDecrement={this.decrementPositiveState('sessionLength')}
            onSessionLengthIncrement={this.incrementState('sessionLength')}
          />
        </Row>
        <Row>
          <Timer
            type={this.state.timerType}
            seconds={this.state.time}
          />
        </Row>
        <Row className="justify-content-center align-items-center">
          <Controls 
            isStarted={this.state.isStarted}
            onToggleStart={this.handleToggleStart}
            onReset={this.handleReset}
          />
        </Row>
      </Container>
    );
  }
}
