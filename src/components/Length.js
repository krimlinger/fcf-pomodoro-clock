import React from 'react';
import { Row, Col } from '../BootstrapGrid';
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowCircleDown, faArrowCircleUp } from '@fortawesome/free-solid-svg-icons';

export default function Length({
  id,
  name,
  value,
  onDecrement,
  onIncrement }) {
  return (
    <>
      <Row>
        <Col>
          <div id={id + "-label"}>
            {name}
          </div>
        </Col>
      </Row>
      <Row>
        <Col xs={4}>
          <Button
            id={id + "-decrement"}
            variant="secondary"
            onClick={onDecrement}
          >
            <FontAwesomeIcon icon={faArrowCircleDown} />
          </Button>
        </Col>
        <Col
          xs={4}
          className="d-flex justify-content-center align-items-center"
        >
          <span id={id + "-length"}>{value}</span>
        </Col>
        <Col xs={4}>
          <Button
            id={id + "-increment"}
            variant="secondary"
            onClick={onIncrement}
          >
            <FontAwesomeIcon icon={faArrowCircleUp} />
          </Button>
        </Col>
      </Row>
    </>
  );
}
