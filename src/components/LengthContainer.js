import React from 'react';
import Length from './Length';
import { Col } from '../BootstrapGrid';

export default function LengthContainer({
  breakLength,
  sessionLength,
  onBreakLengthDecrement,
  onBreakLengthIncrement,
  onSessionLengthDecrement,
  onSessionLengthIncrement,
}) {
  return (
    <>
      <Col>
        <Length
          id="break"
          name="Break Length"
          value={breakLength}
          onDecrement={onBreakLengthDecrement}
          onIncrement={onBreakLengthIncrement}
        />
      </Col>
      <Col>
        <Length
          id="session"
          name="Session Length"
          value={sessionLength}
          onDecrement={onSessionLengthDecrement}
          onIncrement={onSessionLengthIncrement}
        />
      </Col>
    </>
  );
}
