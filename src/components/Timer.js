import React from 'react';
import { Row, Col } from '../BootstrapGrid';
import notificationSound from '../Information_Bell.ogg';

function secondsToClockFormat(sec) {
  const minutes = String(Math.floor(sec / 60));
  const seconds = String(sec % 60);
  return minutes.padStart(2, '0') + ':' + seconds.padStart(2, '0');
}

export default function Timer({ type, seconds }) {
  return (
    <Col>
      <Row>
        <Col>
          <h3 id="timer-label">{type}</h3>
        </Col>
      </Row>
      <Row>
        <Col>
          <h2 id="time-left">{secondsToClockFormat(seconds)}</h2>
          <audio
            id="beep"
            src={notificationSound}
          />
        </Col>
      </Row>
    </Col>
  );
}
