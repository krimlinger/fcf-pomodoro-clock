import React from 'react';
import './Controls.scss';
import Button from 'react-bootstrap/Button';
import { Col } from '../BootstrapGrid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPauseCircle, faPlayCircle, faRecycle } from '@fortawesome/free-solid-svg-icons';

export default function Controls({ isStarted, onToggleStart, onReset }) {
  return (
    <>
      <Col className="d-flex">
        <Button
          variant="secondary"
          id="start_stop"
          className="flex-fill"
          onClick={onToggleStart}
        >
          {isStarted ?
              (<><FontAwesomeIcon icon={faPauseCircle} /> Pause</>):
              (<><FontAwesomeIcon icon={faPlayCircle} /> Start</>)}
        </Button>
      </Col>
      <Col className="d-flex">
        <Button
          variant="secondary"
          id="reset"
          className="flex-fill"
          onClick={onReset}
        >
          <FontAwesomeIcon icon={faRecycle} /> Reset
        </Button>
      </Col>
    </>
  );
}
